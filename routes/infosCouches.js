var express = require('express');
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res, next) {

  console.log(req.body);
  
  // Connexion à la base
  const { Client } = require('pg')
  const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'OASIS',
    password: 'postgres',
    port: 5432,
  })

  client.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });



  // Requête

  const { id_couche } = req.body;

  var requete = `SELECT public."Thematiques".nom_thematique, public."InfoCouches".nom_couche, public."InfoCouches".id_thematique, public."InfoCouches".acquisition, public."InfoCouches".cout, public."InfoCouches".date, public."InfoCouches".description_generale, public."InfoCouches".description_traitement, public."InfoCouches".revisite, public."InfoCouches".lien_applisat 
  FROM public."InfoCouches" 
  JOIN public."Thematiques" ON public."InfoCouches".id_thematique = public."Thematiques".id_themathique
  WHERE public."InfoCouches".id_couche =` + id_couche;

  client.query(requete, (err, result) => {
      console.log(result);
      res.json(result.rows);
      client.end();
  });

});

module.exports = router;