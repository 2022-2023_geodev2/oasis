var express = require('express');
var router = express.Router();



/* GET users listing. */
router.post('/', function(req, res,) {
  
  // Connexion à la base
  const { Client } = require('pg')
  const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'OASIS',
    password: 'postgres',
    port: 5432,
  })

  client.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });


  //Requête


  console.log('body',req.body);
  
  const { id_couche }  = req.body;
  
  var requete = `SELECT public."InfoCouches".data, public."Styles".fill_color, public."Styles".fill_stroke, public."Styles".stroke_color, public."Styles".style_complexe, public."Styles".attribut
  FROM public."Styles"
  JOIN public."InfoCouches" ON public."Styles".id_simple = public."InfoCouches".id_style_simple
  WHERE public."InfoCouches".id_couche =`+id_couche;


  client.query(requete, (err, result) => {
    //console.log(result.rows);
    res.json(result);
    client.end();
  });
  

});

module.exports = router;
