<img src="public/images/Logo_Oasis.png" width="256" height="141">

# Contenu de DOCUMENTATION
### Comment télécharger des images sentinel

### Description des couches
Descriptions générales des couches (descriptions renseignées dans l'interface graphique de l'application).

### Fiche technique EO Browser

### Fonctionnement de la base de données
Explication et schéma de la base de données.

### Informations couches
Listes des couches de l'application et leurs caractéristiques

### Traitement couches OASIS
Description des traitements réalisés sur les couches.

# Démarrage du site encore en développement

## Installation de Node.js
Pour utiliser le site qui est encore en développement, il est nécessaire d'utiliser Node.js
S'il n'est pas encore installé sur votre machine, voici la marche à suivre : 

- Se rendre sur le lien suivant : https://nodejs.org/fr
- Télécharger la version recommandée "pour la plupart des utilisateurs"
- Cliquer sur le fichier téléchargé et autoriser son exécution 
- Suivre la procédure d'installation qui s'affiche en autorisant les conditions générales et en cliquant sur "next"

## Installation des fichiers : 
- Cliquer sur l'onglet download en haut à droite de cette page et sur zip 
- Placer ces fichiers sur un répertoire local de votre ordinateur

## Lancement de Node.js et du site internet
- Dans le menu démarrer, chercher "node" 
- Cliquer sur l'invite de commande de Node.js : `Node.js commande prompt`
- Pour se placer dans le bon répertoire, taper : `cd chemin_vers_le_répertoire`
C'est le répertoire où vous avez installé les fichiers  
Exemple : `cd C:\User\Oasis`
- Taper enter
- Taper ensuite : `npm install`, uniquement lors de la première utilisation
- Taper enter 
- Pour lancer le site, taper : `npm start app.js`
- Taper enter
- Entrer dans le navigateur l'URL : `http://localhost:3000/`

# Fonctionnement de la base de données
*Des informations complémentaires se trouvent dans le fichier DOCUMENTATION\Fonctionnement de la base de données*

## Installation de postgresql: 

https://www.postgresql.org/ 

Toute la base de données est contenue dans un fichier .sql (Il se trouve sur le Gitlab). 

 
## Import du fichier .sql dans postgresql: 

En invite de commande, rentrer la commande suivante :  

`psql -U nom_utilisateur -d OASIS -f chemin_vers_le_sql`
 
## Export du fichier .sql : 

En invite de commande, rentrer la commande suivante : 

`pg_dump -U nom_utilisateur -h localhost OASIS >> chemin_voulu_pour_le_sql`

## Utilisation de PostGresql: 

Pour accéder à une table :  

    Clique droit sur la table 

    Clique sur “view data” 

Les modifications peuvent se faire directement dans la table qui apparait.

## Comment ajouter une couche ?

-	**nom_couche** : nom de la couche
-	**id_couche** : identifiant de la couche. 
Il est rempli automatiquement. Vous pouvez également rentrer vous-même un identifiant pas encore utilisé

-	**id_thematique** : identifiant de la thématique de la couche
-	**acquisition**: satellite(s) ayant fait l’(es) acquisition(s)
-	**cout** : coût des acquisitions et des traitements
-	**date** : date(s) de l’acquisition
-	**data** : GeoJSON de la couche
-	**id_style_simple** : identifiant du style
-	**description_generale** : description générale de la couche
-	**description_traitement**: description des traitements ayant permis d’obtenir la couche
-	**revisite** : temps de revisite du satellite

## Comment ajouter un style ?

On distingue deux types de styles :
-	Styles simples : ils ont un type de contour et un type de remplissage.
-	Styles complexes : ils ont plusieurs types de contours et de remplissages en fonction de l’attribut « DN » dans le GeoJSON source de la couche. Par exemple, les couches « Périmètres brûlés pendant et après incendie » et « Diminution de la surface enneigée dans les Hautes Pyrennées » ont des styles complexes.

Dans le cas d’un **style simple**, il suffit de compléter chaque champ avec les paramètres voulus.

Dans le cas d’un **style complexe** :
On appellera « type » une combinaison de paramètres (fill_color, fill_stroke, stroke_width, stroke_color).
Un style complexe comporte plusieurs types, donc plusieurs lignes dans la base de données. 

-	Remplir les champs fill_color, fill_stroke, stroke_width, stroke_color avec les paramètres voulus
-	Remplir id_simple avec un même identifiant pour tous les types d’un même style
-	Remplir nom_style avec un même nom pour tous les types d’un même style
-	Remplir attribut avec la valeur de « DN » propre au type voulu
-	id_style est un identifiant unique pour chaque type. Il est rempli automatiquement ou vous pouvez le remplir avec un identifiant qui n’est pas encore utilisé




# En cas de problème ou de question
Vous pouvez envoyer un e-mail à :
oasis_ensg@gmail.com

ou appeler/envoyer un SMS au
07 82 69 13 73

<img src="public/images/ENSG.png" width="80" height="87">       <img src="public/images/Logo_Ministère.png" width="123" height="102">
