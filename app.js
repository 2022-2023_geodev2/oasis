var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var coucheRouter = require('./routes/couche');
var afficheNomCoucheRouter = require('./routes/afficheNomsCouches');
var infosCoucheRouter = require('./routes/infosCouches');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/couche', coucheRouter);
app.use('/afficheNomsCouches', afficheNomCoucheRouter);
app.use('/infosCouches', infosCoucheRouter);


module.exports = app;
