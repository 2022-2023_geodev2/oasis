// Imports
let {Map, View} = ol;
let TileLayer = ol.layer.Tile;
let XYZ = ol.source.XYZ;
let VectorSource = ol.source.Vector;
let {transform} = ol.proj;
let GeoJSON = ol.format.GeoJSON;
let VectorLayer = ol.layer.Vector;
let Style = ol.style.Style;
let Fill = ol.style.Fill;
let Stroke = ol.style.Stroke;
let {getUid} = ol.util;


// Définition du centre de la carte
const center = transform([2.4299992825514916,46.53913536360347], 'EPSG:4326', 'EPSG:3857');
const basic_view = new View({
  center: center,
  zoom: 6,
  minZoom : 6,
  maxZoom : 14
})


// Couche image satellite
var image_basemap = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
    url:
      'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Imagery/MapServer/tile/{z}/{y}/{x}',
  }),
});

// Couche carte 1
var map_basemap_1 = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
    url:
      'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
  }),
});

// Couche carte 2
var map_basemap = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012',
    url:
    'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
  }),
});


// Instanciation de la carte
const map = new Map({
  target: 'map',
  layers: [
    image_basemap,// image satellite comme fond de carte par défaut
  ],
  view: basic_view
});

image_basemap.setZIndex(0);

var depart_added = false; // booléen indiquant si les limites de département sont ajoutées


// Construction d'une couche

var liste_id_couches = []; // Liste des id des couches affichées
var liste_id_layer = []; // Liste des id ol des couches affichées
var style_couche = []; // Instanciation de la variable de style pour une couche


function constructeurStyle(result, style_complexe){
/*
Prend les données extraites de la base pour définir un style pour la couche et le booléen indiquant si le style est complexe ou non.
*/

  var style_spe; // Style final de la couche

  if(!style_complexe){ // pour un style simple
    style_spe = [
      new Style({
        fill: new Fill({
          color: result.rows[0].fill_color,
        }),
        stroke: new Stroke({
          color: result.rows[0].stroke_color,
          width: result.rows[0].stroke_width,
        })
      })
    ]

  }else{ // pour un style complexe : on utilise plusieur lignes = plusieurs styles simples
    style_spe = {};
    for(var i=0; i<result.rowCount; i++){
      
      var attribut = result.rows[i].attribut;

      style_spe[attribut] = new Style({
        fill: new Fill({
          color: result.rows[i].fill_color,
        }),
        stroke: new Stroke({
          color: result.rows[i].stroke_color,
          width: result.rows[i].stroke_width,
        })
      })
    }
  }

  //console.log("style: ", style_spe);
  return style_spe;
}

function styleFunction(feature){ // assignation du style à chaque objet en fonction de l'attribut DN
  if(style_complexe){
    //console.log("STYLEFUCNTION : style complexe");
    var dn = feature.getProperties().DN;
    return style_couche[String(dn)];
  }else{
    //console.log("STYLEFUCNTION : style simple");
  }
}

let premiere_couche;
var style_complexe;

/**
 * Construit une couche en lisant le GeoJSON extrait de la BD, an assignant le style approprié
 * @param {string} id_couche - id de la couche
 * @param {object} case_c - div de la couche dans la liste des couches par thématique
 */
function constructeurCouche(id_couche, case_c){

  fetch('/couche', { method: 'post', headers: { 'Content-type': 'application/json' }, body: JSON.stringify({ id_couche }) })
  .then((answer) => answer.json())
  .then((result) => {// chargement des données depuis la BD

    style_complexe = result.rows[0].style_complexe;

    style_couche = constructeurStyle(result, style_complexe);
        
    const sourceVecteur = new VectorSource({ // lecture du GeoJSON
      features: new GeoJSON().readFeatures(result.rows[0].data),
    });

    console.log("style_complexe: ", style_complexe);
    if(!style_complexe){ // ajout du style à la couche pour un style simple
      var couche = new VectorLayer({
        source: sourceVecteur,
        style : style_couche
      });
    }else{ // ajout du style à la couche pour un style complexe
      var couche = new VectorLayer({
        source: sourceVecteur,
        style : styleFunction
      });
    }

    if(id_couche != 0){ // pour toutes les couches sauf celle des départements
      map.addLayer(couche); // ajout de la couche
      map.getView().fit(sourceVecteur.getExtent()); // centrage et zoom sur la couche
      couche.setZIndex(2); // ajout de la couche par-dessus le fond de carte 
      creer_div_couche(case_c, result.rows[0].fill_color, sourceVecteur, couche); // ajout de la couche dans le gestionnaire

    }else{ // pour la couche des départements dont l'index est 0
      map.addLayer(couche);
      couche.setZIndex(1);
    }

    // Ajout de l'id de la couche à la liste des id de couches affichées
    liste_id_layer.push(getUid(couche));
    // Ajout de la couche à la liste des couches affichées
    liste_id_couches.push(id_couche);
    // Écoute l'événement "change:length" de la collection de couches de la carte
    map.getLayers().on("change:length", centrerSurPremiereCouche);
  });
}

/**
 * Centre sur la première couche séléctionnée
 */
function centrerSurPremiereCouche() {
  if (premiere_couche) {
    map.getView().fit(premiere_couche.getSource().getExtent());
  }
}

/**
 *   Supprime la couche de la carte et du gestionnaire de couches
 * @param {string} id_couche - id de la couche
 * @param {object} case_c - div de la couche dans la liste des couches par thématique
 */
function supprime_couche(id_couche, case_c){

  var index_couche = liste_id_couches.indexOf(id_couche); // récupère l'id de la couche

  // Suppression de la couche dans la carte
  var toutes_couches = map.getAllLayers(); // récupèr la liste de toutes les couches
  toutes_couches.forEach(couche => { 
    if(getUid(couche) == liste_id_layer[index_couche]){ //récupère l'id ol de la couche
      map.removeLayer(couche); //supprime la couche de la carte
    }
  })
  if(id_couche != 0){
    var div_couche = document.querySelector('.couche[data-id="' + id_couche + '"]');
    div_couche.remove(); // supprime la couche dans le gestionnaire
    case_c.style.backgroundColor = 'rgb(192, 236, 255)';
  }
    

  // Suppression de la couche dans les listes des couches affichées
  liste_id_layer.splice(index_couche, 1); 
  liste_id_couches.splice(index_couche, 1);

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Carte

// Création des interactions avec les  boutons de fond de carte et de la couche des départements
const choix_img_sat = document.getElementById('choix_img_sat');
choix_img_sat.addEventListener("change", verifCocheImg);

const choix_carte = document.getElementById('choix_carte');
choix_carte.addEventListener("change", verifCocheCarte);

const choix_depart = document.getElementById('choix_depart');
choix_depart.addEventListener("change", verifCocheDepart);

/**
 * Ajout et suppresion du fond de carte OSM
 * @param {object} event - Évènement
 */
function verifCocheCarte(event){

  event.preventDefault();
  if(choix_carte.checked){
    map.addLayer(map_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(image_basemap);
    choix_img_sat.checked = false;
  }
  if(!choix_carte.checked){
    map.addLayer(image_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(map_basemap);
    choix_img_sat.checked = true;
  }
}

/**
 * Ajout et suppresion du fond de carte satellite
 * @param {object} event - Évènement
 */
function verifCocheImg(event){

  event.preventDefault();
  if(choix_img_sat.checked){
    map.addLayer(image_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(map_basemap);
    choix_carte.checked = false;
  }
  if(!choix_img_sat.checked){
    map.addLayer(map_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(image_basemap);
    choix_carte.checked = true;
  }
  compteur_basemap += 1;
}

/**
 * Ajout et suppresion des limites de départements
 * @param {object} event - Évènement
 */
function verifCocheDepart(event){

  event.preventDefault();
  if(!choix_depart.checked){
    supprime_couche(0);
    depart_added = false;
  }
  if(choix_depart.checked && depart_added == false){
    constructeurCouche(0);
    depart_added = true;
  }
}

// Choix de la thématique
clique_thematique();

/**
 * Permet d'afficher la div qui contient la liste des couches suite au clique utilisateur sur une des thématiques Applisat
 */
function clique_thematique(){
  var collapseDivs = document.querySelectorAll('[data-target]');
  var lastDisplayedDiv = 'none';

  collapseDivs.forEach(div => {
      var targetId = div.getAttribute('data-target');
      var targetDiv = document.querySelector(targetId);

      if (targetDiv.style.display === 'block') {
        lastDisplayedDiv = targetDiv;
      }

      div.addEventListener('click', () => {
        // Cache la dernière div affichée
        if (lastDisplayedDiv !== 'none' && lastDisplayedDiv !== targetDiv) {
          lastDisplayedDiv.style.display = 'none';
          var lastDiv = document.querySelector(`[data-target="#${lastDisplayedDiv.id}"]`);
          lastDiv.classList.remove('clicked');
        }
        if (targetDiv.style.display === 'block') {
          targetDiv.style.display = 'none';
          div.classList.remove('clicked');
        }
        else {
          targetDiv.style.display = 'block';
          targetDiv.style.backgroundColor = 'white';
          div.classList.add('clicked');
          lastDisplayedDiv = targetDiv;
          // Parcourt toutes les divs et supprimer la classe 'clicked'
          collapseDivs.forEach((otherDiv) => {
            if (otherDiv !== div) {
              otherDiv.classList.remove('clicked');
            }
          });
          // Récupére l'id de la thématique correspondante
          var id_thematique = div.getAttribute('id');
          // Vide la div de tous les éléments ayant la classe 'bouton_couche'
          var elementsASupprimer = targetDiv.querySelectorAll('.bouton_couche');
          elementsASupprimer.forEach((element) => {
            targetDiv.removeChild(element);
          });
          affiche_nom(id_thematique, targetDiv);
        }
      });
    });
}

/**
 * Effectue une requête afin de récupérer chaque nom et id des couches qui sont à afficher pour chaque thématiques choisies
 * @param {string} id_thematique - id de la thématique 
 * @param {object} div - div de la thématique qui va contenir les noms de couches
 */
function affiche_nom(id_thematique, div){
  // Vérifier si les noms de couches ont déjà été chargés
  if (div.getAttribute('data-loaded') === 'true') {
    return;
  }

  fetch('/afficheNomsCouches', { method: 'post', headers: { 'Content-type': 'application/json' }, body: JSON.stringify({ id_thematique }) })
  .then((answer) => answer.json())
  .then((result) => {

    for (let i = 0; i < result.length; i++) {
      div.innerHTML += "<div class='bouton_couche' id=" + result[i].id_couche + " name='data' nom_couche=\"" + result[i].nom_couche + "\" data-show='false' data-hover='false'>" + result[i].nom_couche + "</div>";
    }

    // Récupération de l'identifiant de la couche selectionnée apr l'utilisateur 
    const cases_couches = document.getElementsByName('data');

    cases_couches.forEach(case_c => {
      case_c.setAttribute('layer_added', 'false');
      case_c.setAttribute('data-show', 'false');

      case_c.addEventListener("click", function(event){
        event.preventDefault();  
        select_couche(case_c);
        moveDiv();
      });
    });
    function select_couche(case_c){
      var id_couche = case_c.id;
      var layer_added = case_c.getAttribute('layer_added');
      var data_show = case_c.getAttribute('data-show');
      if(layer_added == 'false' && data_show == 'false'){
        case_c.setAttribute('layer_added', 'true');
        constructeurCouche(id_couche, case_c);
        case_c.style.backgroundColor = 'rgb(128, 174, 226)';
      }else{
        case_c.setAttribute('layer_added', 'false');
        case_c.setAttribute('data-show','false');
        supprime_couche(id_couche, case_c);
      };
    }
  })
}

/**
 * Crée une div dans l'onglet de gestion des couches contenant : le nom et la couleur de la couche, des images pour les différentes
 * intéractions et une barre (input range) pour gérer la transparence de la couche.
 * @param {object} div - div de la couche dans la liste de couche de la thématique
 * @param {string} color - couleur de la couche
 * @param {object} sourceVecteur - vecteur de la couche
 * @param {object} nvL_couche - couche (avec vecteur et style)
 */
function creer_div_couche(div, color, sourceVecteur, nvL_couche) {

  var id_couche = div.id;
  var nom_couche = div.getAttribute('nom_couche');
  // Vérifier si la div_couche est déjà affichée
  var div_couche = document.querySelector('.couche[data-id="' + id_couche + '"]');

  div_couche = document.createElement('div');
  div_couche.classList.add('couche');
  div_couche.setAttribute('data-id', id_couche);
        
  design_div(div_couche, div, id_couche, nom_couche, color, sourceVecteur, nvL_couche);
  gestion_couche.appendChild(div_couche);
}

/**
 * Design la div créée par la fonction creer_div_couche. Création de chaque élément de la div : le nom et la couleur de la couche, des 
 * images pour les différentes intéractions et une barre (input range) pour gérer la transparence de la couche.
 * @param {object} div - div de la couche dans la gestion des couches
 * @param {object} case_c - div de la couche dans la liste des couches par thématique
 * @param {string} id_couche - id de la couche
 * @param {string} nom_couche - nom de la couche
 * @param {string} color - couleur de la couche
 * @param {object} sourceVecteur - vecteur de la couche
 * @param {object} nvL_couche - couche (avec vecteur et style)
 */
function design_div(div, case_c, id_couche, nom_couche, color, sourceVecteur, nvL_couche){
  //div.style.border = '2px solid white';
  div.style.borderRadius = '5px';
  div.style.margin = '5px';
  div.style.marginTop = '10px';
  div.style.height = '120px';
  div.style.display = 'grid';
  div.style.gridTemplateColumns = 'repeat(10, 1fr)';
  div.style.gridTemplateRows = 'repeat(6, 1fr)';
  div.style.boxShadow = '0 5px 5px rgba(0, 0, 0, 0.2)';
  div.style.backgroundColor = '#f5f5f5';

  // Couleur de la couche
  var couleur_couche = document.createElement('div');
  couleur_couche.style.gridColumn = '1 / 3';
  couleur_couche.style.gridRow = '1 / 3';
  couleur_couche.style.backgroundColor = color;
  couleur_couche.style.width = '20px';
  couleur_couche.style.height = '20px';
  couleur_couche.style.borderRadius = '50%';
  couleur_couche.style.margin = 'auto';
  couleur_couche.style.border = '2px solid rgba(53, 53, 53)';
  //couleur_couche.style.position = 'absolute';
  div.appendChild(couleur_couche);

  // Nom de la couche
  var nom_div = document.createElement('div');
  nom_div.style.gridColumn = '3 / 11';
  nom_div.style.gridRow = '1 / 3';
  nom_div.style.fontSize = '12px';
  nom_div.style.margin = '5px';
  nom_div.style.fontWeight = '600';
  nom_div.textContent = nom_couche;
  div.appendChild(nom_div);

  // Barre transparence
  var transparence = document.createElement("input");
  transparence.setAttribute('type', 'range');
  transparence.setAttribute('min', '0');
  transparence.setAttribute('max', '1');
  transparence.setAttribute('value', '0');
  transparence.setAttribute('step', '0.01');
  transparence.style.gridRow = '4';
  transparence.style.gridColumn = '2 / 10';
  transparence.style.width = '100%';
  transparence.style.height = '50%';
  transparence.style.marginTop = 'auto';
  transparence.style.marginBottom = 'auto';
  transparence.style.borderRadius = '10px';
  transparence.style.backgroundColor = '#bfe2ff';
  transparence.style.appearance = 'none';
  transparence.style.outline = 'none';
  transparence.style.webkitAppearance = 'none';
  transparence.style.mozAppearance = 'none';
  transparence.style.alignItems = 'center';
  div.appendChild(transparence);

  // Label 0%
  var label_0 = document.createElement("label");
  label_0.textContent = "0%";
  label_0.style.gridColumn = '1';
  label_0.style.gridRow = '4';
  label_0.style.margin = '0';
  label_0.style.fontSize = '10px';
  label_0.style.fontWeight = '600';
  label_0.style.alignItems = 'center';
  label_0.style.justifyContent = 'right';
  div.appendChild(label_0);
  
  // Label 100%
  var label_100 = document.createElement("label");
  label_100.textContent = "100%";
  label_100.style.gridColumn = '10';
  label_100.style.gridRow = '4';
  label_100.style.margin = '0';
  label_100.style.fontSize = '10px';
  label_100.style.fontWeight = '600';
  label_0.style.alignItems = 'center';
  label_0.style.justifyContent = 'left';
  div.appendChild(label_100);

  // Bouton Information
  var img_info = document.createElement("img");
  img_info.src = "images/information.png";
  img_info.style.gridRow = ' 5 / 7';
  img_info.style.gridColumn = '2 / 4';
  img_info.style.width = '60%';
  img_info.style.height = '60%';
  img_info.style.objectFit = 'contain';
  img_info.style.margin = 'auto';
  div.appendChild(img_info);

  // Visualisation
  var img_visu = document.createElement("img");
  img_visu.src = "images/oeil_ouvert.png";
  img_visu.style.gridRow = '5 / 7';
  img_visu.style.gridColumn = '4 / 6';
  img_visu.style.width = '60%';
  img_visu.style.height = '60%';
  img_visu.style.objectFit = 'contain';
  img_visu.style.margin = 'auto';
  div.appendChild(img_visu);

  // Recentrer
  var img_center = document.createElement("img");
  img_center.src = "images/center.png";
  img_center.style.gridRow = '5 / 7';
  img_center.style.gridColumn = '6 / 8';
  img_center.style.width = '60%';
  img_center.style.height = '60%';
  img_center.style.objectFit = 'contain';
  img_center.style.margin = 'auto';
  div.appendChild(img_center);

  // Poubelle
  var img_bin = document.createElement("img");
  img_bin.src = "images/recycle-bin.png";
  img_bin.style.gridRow = '5 / 7';
  img_bin.style.gridColumn = '8 / 10';
  img_bin.style.width = '60%';
  img_bin.style.height = '60%';
  img_bin.style.objectFit = 'contain';
  img_bin.style.margin = 'auto';
  div.appendChild(img_bin);

  interaction(div, case_c, id_couche, nom_couche, transparence, img_info, img_visu, img_center, img_bin, sourceVecteur, nvL_couche);
}

/**
 * Gestion des interactions dans la div de la couche dans la gestion des couches (transparence, infos, visibilité, recentrage et 
 * suppression)
 * @param {object} div - div de la couche dans la gestion des couches
 * @param {object} case_c - div de la couche dans la liste des couches par thématique
 * @param {string} id_couche - id de la couche
 * @param {string} nom_couche - nom de la couche
 * @param {object} transparence - input range pour la barre de transparence
 * @param {object} img_info - image pour l'onglet informatif
 * @param {object} img_visu - image pour la visibilté (show/hide)
 * @param {object} img_center - image pour le recentrage
 * @param {object} img_bin - image pour la poubelle (drop des couches)
 * @param {object} sourceVecteur - vecteur de la couche
 * @param {object} nvL_couche - couche (avec vecteur et style)
 */
function interaction(div, case_c, id_couche, nom_couche, transparence, img_info, img_visu, img_center, img_bin, sourceVecteur, nvL_couche){
   
  // Transparence
  transparence.addEventListener('change', function() {
    var opacite = 1-(transparence.value);
    nvL_couche.setOpacity(opacite);
  });
  // Poubelle
  img_bin.addEventListener('click', function() {
    div.click();
    supprime_couche(id_couche, case_c);
  });
  // Recentrer
  img_center.addEventListener('click', function(){
    map.getView().fit(sourceVecteur.getExtent());
  });
  // Visible ou non
  var compteur_visible = 0
  img_visu.addEventListener('click', function() {
    compteur_visible+=1;

    if(compteur_visible%2==0){
      nvL_couche.setVisible(true);
      img_visu.src = "images/oeil_ouvert.png";
    }else{
      nvL_couche.setVisible(false);
      img_visu.src = "images/oeil_barre.png";
    }
  });
  // Informations
  img_info.addEventListener('click',function(){
    informations(id_couche, nom_couche);
  });

  
}
 
/**
 * Permet de déplacer les couches dans l'onglet de gestion des couches
 */
function moveDiv(){
  var gestionCoucheDiv = document.getElementById('gestion_couche');
  var selectedDiv = null;
  var initialX = null;
  var initialY = null;
  var targetDiv = null;
  var targetIndex = -1;

  gestionCoucheDiv.addEventListener('mousedown', function(e) {
    if (e.target.className.indexOf('couche') !== -1) {
      selectedDiv = e.target;
      initialX = e.clientX;
      initialY = e.clientY;
      targetDiv = null;
      targetIndex = -1;
      selectedDiv.style.cursor = "move";
    }
  });

  gestionCoucheDiv.addEventListener('mousemove', function(e) {
    if (selectedDiv) {
      var currentX = e.clientX;
      var currentY = e.clientY;
      selectedDiv.style.left = currentX - initialX + 'px';
      selectedDiv.style.top = currentY - initialY + 'px';
  
      // Trouve la div sous laquelle la div sélectionnée est déplacée
      var divs = gestionCoucheDiv.querySelectorAll('.couche');

      for (var i = 0; i < divs.length; i++) {
        var div = divs[i];
        if (div === selectedDiv){
          continue;
        }
        var rect = div.getBoundingClientRect();
        if (currentX > rect.left && currentX < rect.right && currentY > rect.top && currentY < rect.bottom) {
          targetDiv = div;
          targetIndex = i;
          break;
        }
      }
      if (targetDiv) {
        var divs = gestionCoucheDiv.querySelectorAll('.couche');
        var selectedIndex = Array.prototype.indexOf.call(divs, selectedDiv);
        
        if (selectedIndex < targetIndex) {
          targetDiv.parentNode.insertBefore(selectedDiv, targetDiv.nextSibling);
          var divs = gestionCoucheDiv.querySelectorAll('.couche');
          var selectedIndex = Array.prototype.indexOf.call(divs, selectedDiv);
          var targetIndex = Array.prototype.indexOf.call(divs, targetDiv);

          switchCouches(liste_id_couches, selectedIndex, targetIndex);
        } else {
          targetDiv.parentNode.insertBefore(selectedDiv, targetDiv);
          var divs = gestionCoucheDiv.querySelectorAll('.couche');
          var selectedIndex = Array.prototype.indexOf.call(divs, selectedDiv);
          var targetIndex = Array.prototype.indexOf.call(divs, targetDiv);

          // Récupération des indices dans la liste des layers
          var position_liste_layer_a = liste_id_couches.indexOf(selectedIndex);
          var index_layer_a = liste_id_layer[position_liste_layer_a];
          var position_liste_layer_b = liste_id_couches.indexOf(targetIndex);
          var index_layer_b = liste_id_layer[position_liste_layer_b];

          switchCouches(liste_id_couches, selectedIndex, targetIndex);
          switchCouches(liste_id_couches, index_layer_a, index_layer_b);
        }
      }
    }
  });

  gestionCoucheDiv.addEventListener('mouseup', function(e) {
    if (selectedDiv) {
      selectedDiv.style.transform = "";
      selectedDiv.style.cursor = "";
      selectedDiv = null;
      initialX = null;
      initialY = null;
      targetDiv = null;
      targetIndex = -1;
      var test = liste_id_couches.join();

      var nb_couches = liste_id_layer.length;
      var toutes_couches = map.getAllLayers();
      var compteur = -1;
      
      liste_id_layer.forEach(id_layer => {
        toutes_couches.forEach(couche => {
          if(couche.getUid == id_layer){
            couche.setZIndex(nb_couches - compteur);
          }
        })
        compteur += 1;
      })
      }
  });
}

/**
 * Permet de changer l'ordre des couches dans la liste liste_id_couches
 * @param {object} tab - Tableau des indices des couches
 * @param {number} couche1 - Couche 1 à échanger
 * @param {number} couche2 - Couche 2 à échanger
 */
function switchCouches(tab, couche1, couche2) {  
  var tmp = tab[couche1];
  tab[couche1] = tab[couche2];
  tab[couche2] = tmp;
}

/**
 * Récupère toutes les informations des couches et remplit l'onglet informatif
 * @param {string} id_couche - id de la couche
 * @param {object} modalBodyDiv - body du modal Bootstrap
 */
function recup_infos(id_couche, modalBodyDiv){

  fetch('/infosCouches', { method: 'post', headers: { 'Content-type': 'application/json' }, body: JSON.stringify({ id_couche }) })
  .then((answer) => answer.json())
  .then((result) => {

    // Div thématique
    var divThematique = document.createElement('div');
    divThematique.innerHTML = '<b>Thématique :</b>';
    var thematique = document.createElement('div');
    thematique.innerHTML = result[0].nom_thematique;
    divThematique.appendChild(thematique);
    modalBodyDiv.appendChild(divThematique);

    // Ligne
    modalBodyDiv.appendChild(document.createElement('hr'));

    // Div date
    var divDate = document.createElement('div');
    divDate.innerHTML = '<b>Date :</b>';
    var date = document.createElement('div');
    date.innerHTML = result[0].date;
    divDate.appendChild(date);
    modalBodyDiv.appendChild(divDate);

    // Ligne
    modalBodyDiv.appendChild(document.createElement('hr'));
    
    // Satellite/Bande
    var divAcquisition = document.createElement('div');
    divAcquisition.innerHTML = '<b>Satellite/Bande :</b>';
    var acquisition = document.createElement('div');
    acquisition.innerHTML = result[0].acquisition;
    divAcquisition.appendChild(acquisition);
    modalBodyDiv.appendChild(divAcquisition);

    // Ligne
    modalBodyDiv.appendChild(document.createElement('hr'));

    // Coût
    var divCout = document.createElement('div');
    divCout.innerHTML = '<b>Coût :</b>';
    var cout = document.createElement('div');
    cout.innerHTML = result[0].cout;
    divCout.appendChild(cout);
    modalBodyDiv.appendChild(divCout);

    // Ligne
    modalBodyDiv.appendChild(document.createElement('hr'));

    // Revisite
    var divRevisite = document.createElement('div');
    divRevisite.innerHTML = '<b>Revisite :</b>';
    var revisite = document.createElement('div');
    revisite.innerHTML = result[0].revisite;
    divRevisite.appendChild(revisite);
    modalBodyDiv.appendChild(divRevisite);

    // Ligne
    modalBodyDiv.appendChild(document.createElement('hr'));

    // Description générale
    var divDescGen = document.createElement('div');
    divDescGen.innerHTML = '<b>Description générale :</b>';
    var description_generale = document.createElement('div');
    description_generale.innerHTML = result[0].description_generale;
    divDescGen.appendChild(description_generale);
    modalBodyDiv.appendChild(divDescGen);

    // Ligne
    modalBodyDiv.appendChild(document.createElement('hr'));

    // Description du traitement
    var divDescTrait = document.createElement('div');
    divDescTrait.innerHTML = '<b>Description du traitement :</b>';
    var description_traitement = document.createElement('div');
    description_traitement.innerHTML = result[0].description_traitement;
    divDescTrait.appendChild(description_traitement);
    modalBodyDiv.appendChild(divDescTrait);

    // Ligne
    modalBodyDiv.appendChild(document.createElement('hr'));

    // Lien applisat
    var divLienApplisat = document.createElement('div');
    divLienApplisat.innerHTML = '<b>Lien Applisat :</b>';
    var lienApplisat = document.createElement('div');
    lienApplisat.innerHTML = '<p><a href="'+result[0].lien_applisat+'" class="tooltip-test" title="Tooltip" target="_blank">Fiche applisat</a></p>'
    divLienApplisat.appendChild(lienApplisat);
    modalBodyDiv.appendChild(divLienApplisat);
  });
}

/**
 * Construit un modal Bootstrap afin d'y mettre les informations concernant les couches
 * @param {string} id_couche 
 * @param {string} nom_couche 
 */
function informations(id_couche, nom_couche){

  // Créer un élément div pour contenir le modal
  var modalDiv = document.createElement('div');

  // Ajouter les classes Bootstrap nécessaires pour un modal
  modalDiv.classList.add('modal', 'fade');
  modalDiv.setAttribute('tabindex', '-1');
  modalDiv.setAttribute('role', 'dialog');
  modalDiv.setAttribute('aria-labelledby', 'exampleModalLabel');
  modalDiv.setAttribute('aria-hidden', 'true');

  // Créer un élément div pour la fenêtre modale
  var modalDialogDiv = document.createElement('div');
  modalDialogDiv.classList.add('modal-dialog', 'modal-lg', 'bg-white', 'rounded');
  modalDialogDiv.setAttribute('role', 'document');

  // Créer un élément div pour le contenu du modal
  var modalContentDiv = document.createElement('div');
  modalContentDiv.classList.add('modal-content');

  // Ajouter un élément div pour le header
  var modalHeaderDiv = document.createElement('div');
  modalHeaderDiv.classList.add('modal-header');

  // Ajouter un titre pour le header
  var modalTitle = document.createElement('h5');
  modalTitle.classList.add('modal-title');
  modalTitle.innerHTML = nom_couche;
  modalHeaderDiv.appendChild(modalTitle);

  // Ajouter un bouton pour fermer le modal
  var modalCloseButton = document.createElement('button');
  modalCloseButton.classList.add('close');
  modalCloseButton.setAttribute('type', 'button');
  modalCloseButton.setAttribute('data-dismiss', 'modal');
  modalCloseButton.setAttribute('aria-label', 'Close');
  var modalCloseIcon = document.createElement('span');
  modalCloseIcon.setAttribute('aria-hidden', 'true');
  modalCloseIcon.innerHTML = '&times;';
  modalCloseButton.appendChild(modalCloseIcon);
  modalHeaderDiv.appendChild(modalCloseButton);

  // Ajouter le header au contenu du modal
  modalContentDiv.appendChild(modalHeaderDiv);

  // Ajouter un élément div pour le corps du modal
  var modalBodyDiv = document.createElement('div');
  modalBodyDiv.classList.add('modal-body');
  recup_infos(id_couche, modalBodyDiv);
  modalContentDiv.appendChild(modalBodyDiv);

  // Ajouter les éléments au modalDialogDiv
  modalDialogDiv.appendChild(modalContentDiv);

  // Ajouter les éléments au modalDiv
  modalDiv.appendChild(modalDialogDiv);

  // Ajouter le modalDiv à la fin du body
  document.body.appendChild(modalDiv);

  // Ouvrir le modal
  var modal = new bootstrap.Modal(modalDiv);
  modal.show();

}