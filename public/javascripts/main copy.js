// import './style.css';

let {Map, View} = ol;
let TileLayer = ol.layer.Tile;
let XYZ = ol.source.XYZ;
let VectorSource = ol.source.Vector;
let {transform} = ol.proj;
let GeoJSON = ol.format.GeoJSON;
let VectorLayer = ol.layer.Vector;
let Style = ol.style.Style;
let Fill = ol.style.Fill;
let Stroke = ol.style.Stroke;
let {getUid} = ol.util;
//import { string } from 'mathjs';



// Définition du centre de la carte
const center = transform([2.4299992825514916,46.53913536360347], 'EPSG:4326', 'EPSG:3857');
const basic_view = new View({
  center: center,
  zoom: 6,
  minZoom : 6,
  maxZoom : 14
})


// Couche image satellite
var image_basemap = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
    url:
      'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Imagery/MapServer/tile/{z}/{y}/{x}',
  }),
});

// Couche carte 1
var map_basemap_1 = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles © <a href="https://services.arcgisonline.com/ArcGIS/' +
      'rest/services/World_Topo_Map/MapServer">ArcGIS</a>',
    url:
      'https://server.arcgisonline.com/ArcGIS/rest/services/' +
      'World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
  }),
});

// Couche carte 2
var map_basemap = new TileLayer({
  source: new XYZ({
    attributions:
      'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012',
    url:
    'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
  }),
});

// Couche départements
const style_depart = [
  new Style({
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0)',
    }),
    stroke: new Stroke({
      color: 'rgba(0, 0, 0)',
      width: 1,
    })
  })
]

var carte_departements = new VectorLayer({
  source: new VectorSource({
    format: new GeoJSON(),
    //url: 'couches_systeme/departements.geojson', 
    url : '/accueil',
  }),
  style : style_depart
});

carte_departements.setZIndex(1);



// Instanciation de la carte
const map = new Map({
  target: 'map',
  layers: [
    image_basemap,
    //carte_departements,
  ],
  view: basic_view
});

var depart_added = true;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STYLES

// Carte des départements

// Surface brûlée
/*const surf_brulee = [
  new Style({
    fill : new Fill({
      color:'rgb(178,34,34)',
      stroke : new Stroke({
        color:"rgb(178,34,34)"
      })
    })
  })
]*/

const surf_brulee = {
  0 : new Style({
    fill : new Fill({
      color : 'rgb(178,34,34)'
    }),
    stroke : new Stroke({
      color : 'rgb(178,34,34)',
    })
  }),
  1 : new Style({
    fill : new Fill({
      color : 'brown'
    }),
    stroke : new Stroke({
      color : 'brown',
    })
  }),
}

// Inondations
const inondations = [
  new Style({
    fill : new Fill({
      color:'rgb(0,255,228)',
      stroke : new Stroke({
        color:"rgb(0,255,228)"
      })
    })
  })
]

// Zones d'eau
const zonesEau = [
  new Style({
    fill : new Fill({
      color:'rgb(0,108,255)',
      stroke : new Stroke({
        color:"rgb(0,108,255)"
      })
    })
  })
]

// Forêts
const forets = [
  new Style({
    fill : new Fill({
      color:'rgb(0,255,73)',
      stroke : new Stroke({
        color:"rgb(0,255,73)"
      })
    })
  })
]

// Trait de cote
const trait_cote = [
  new Style({
    fill : new Fill({
      color:'#F7FF00',
      stroke : new Stroke({
        color:"#F7FF00",
        width: 10,
      })
    }),
    stroke : new Stroke({
      color:"#F7FF00",
      width: 3
    })
  })
]

// Chlorophylle
const chloro = [
  new Style({
    fill : new Fill({
      color:'#3EFF00',
      stroke : new Stroke({
        color:"#3EFF00"
      })
    })
  })
]

// Photosynthèse
const photosynthese = [
  new Style({
    fill : new Fill({
      color:'#FFAA00',
      stroke : new Stroke({
        color:"#FFAA00"
      })
    })
  })
]

// Neige

const neige = {
  0 : new Style({
    fill : new Fill({
      color : 'rgb(255, 255, 255)'
    }),
    stroke : new Stroke({
      color : 'rgb(255, 255, 255)',
    })
  }),
  1 : new Style({
    fill : new Fill({
      color : 'brown'
    }),
    stroke : new Stroke({
      color : 'brown',
    })
  }),
}



// Intégrité
const integrite = [
  new Style({
    fill : new Fill({
      color:'#08AF3A',
      stroke : new Stroke({
        color:"#08AF3A"
      })
    })
  })
]

// Friches
const friche = [
  new Style({
    fill : new Fill({
      color:'#BEE635',
      stroke : new Stroke({
        color:"#BEE635"
      })
    })
  })
]

// Zone de verdure en ville
const verdure = [
  new Style({
    fill : new Fill({
      color:'#56D914',
      stroke : new Stroke({
        color:"#56D914"
      })
    })
  })
]

// Stress hydrique
const stress_hydrique = [
  new Style({
    fill : new Fill({
      color:'#7ADEEE',
      stroke : new Stroke({
        color:"#1ED7F4"
      })
    })
  })
]

function styleFunction(feature){
  console.log("feature.getProperties(): "+feature.getProperties().DN);
  return feature.getProperties().DN;
}


function style_specifique(case_c, data_couche, style){
  fetch(data_couche)
  .then((response) => response.json())
  .then((r)=>{
    const sourceVecteur = new VectorSource({
      features: new GeoJSON().readFeatures(r), 
    });

    // Création de la couche
    nvL_couche = new VectorLayer({
      source: sourceVecteur,
      style : styleFunction,
    });
    map.addLayer(nvL_couche);
    nvL_couche.setZIndex(2);
    // Permet de récupérer la couleur de la couche afin d'afficher un petit cercle de cette couleur à côté du nom de la couche
    var color = 'rgb(255, 255,255)';
    affichage_couche(case_c, color, sourceVecteur, nvL_couche);

    // Ajout de l'id de la couche à la liste des id de couches affichées
    liste_id_couches.push(getUid(nvL_couche));
    // Ajout de la couche à la liste des couches affichées
    liste_couches.push(data_couche);
  })
  
}


const list_var_styles = [surf_brulee, inondations, zonesEau, forets, trait_cote, chloro, photosynthese, neige, integrite, friche, verdure, stress_hydrique];
const list_noms_styles = ["surf_brulee", "inondations", "zonesEau", "forets", "trait_cote", "chloro", "photosynthese", "neige", "integrite", "friche", "verdure", "stress_hydrique"];
const couches_styles_spe = ['couches/1_1.geojson','couches/3_1.geojson'];


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CHARGEMENT DES COUCHES
var liste_couches = [];
var liste_id_couches = [];
var nvL_couche;

function zoom_sur_source(sourceVecteur){
  map.getView().fit(sourceVecteur.getExtent());
}

function charge_couche(case_c, data_couche, style){
  

  console.log("Ajout de la couche "+data_couche+"; style: "+style);

  //Gestion des couches dont le style comprend plusieurs couleurs Ex: diminution de la couv neigeuse
  if(couches_styles_spe.includes(data_couche)){
    nvL_couche = style_specifique(case_c, data_couche, style);
  }else{
  
    // Récupération du style en fonction du nom :
    let style_select;
    for(var i=0; i<list_noms_styles.length; i++){
      if(style == list_noms_styles[i]){
        style_select = list_var_styles[i];
      }
    }

    // Création de la source
    let sourceVecteur = new VectorSource({
      format: new GeoJSON(),
      url: data_couche, 
    })
    sourceVecteur.on("featuresloadend", function() {
      zoom_sur_source(sourceVecteur);
    })

    // Création de la couche
    nvL_couche = new VectorLayer({
      source: sourceVecteur,
      style : style_select
    });
    map.addLayer(nvL_couche);
    nvL_couche.setZIndex(2);

    // Permet de récupérer la couleur de la couche afin d'afficher un petit cercle de cette couleur à côté du nom de la couche
    var color;
    if(style_select != neige){
      var layerStyle = nvL_couche.getStyle()[0];
      color = layerStyle.getFill().getColor();
    }else{
      color = 'rgb(255, 255, 255)';
    }
    
    affichage_couche(case_c, color, sourceVecteur, nvL_couche);

    // Ajout de l'id de la couche à la liste des id de couches affichées
    liste_id_couches.push(getUid(nvL_couche));
    // Ajout de la couche à la liste des couches affichées
    liste_couches.push(data_couche);
  }

}

function supprime_couche(data_couche){
  var index_couche = liste_couches.indexOf(data_couche);

  // Suppression de la couche dans la carte
  var toutes_couches = map.getAllLayers();
  toutes_couches.forEach(couche => {
    if(getUid(couche) == liste_id_couches[index_couche]){
      map.removeLayer(couche);
    }
  })

  // Suppression de la couche dans les listes
  liste_couches.splice(index_couche, 1);
  liste_id_couches.splice(index_couche, 1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Séléction des couches

const cases_couches = document.getElementsByName("couche");

cases_couches.forEach(case_c => {
  case_c.addEventListener("change", select_couche)
});



function select_couche(event){
  event.preventDefault();
  var unchecked = 0;

  cases_couches.forEach(case_c=>{
    let id_couche = case_c.id;
    let args = id_couche.split(',');
    let data_couche = args[0];
    let style = args[1];

    if(case_c.checked){

      if(!liste_couches.includes(data_couche)){
        //console.log("data_couche: ", data_couche, "; style: ", style);
        charge_couche(case_c, data_couche, style);
      }
      

    }else{
      unchecked+=1;
      if(liste_couches.includes(data_couche)){
        supprime_couche(data_couche);
      }
    }

    if(unchecked==4){
      map.setView(basic_view);
    }

  })  
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Carte

const choix_img_sat = document.getElementById('choix_img_sat');
choix_img_sat.addEventListener("change", verifCocheImg);

const choix_carte = document.getElementById('choix_carte');
choix_carte.addEventListener("change", verifCocheCarte);

const choix_depart = document.getElementById('choix_depart');
choix_depart.addEventListener("change", verifCocheDepart);

function verifCocheCarte(event){
  event.preventDefault();
  if(choix_carte.checked){
    map.addLayer(map_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(image_basemap);
    choix_img_sat.checked = false;
  }
  if(!choix_carte.checked){
    map.addLayer(image_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(map_basemap);
    choix_img_sat.checked = true;
  }
}

function verifCocheImg(event){
  event.preventDefault();
  if(choix_img_sat.checked){
    map.addLayer(image_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(map_basemap);
    choix_carte.checked = false;
  }
  if(!choix_img_sat.checked){
    map.addLayer(map_basemap);
    map_basemap.setZIndex(0);
    map.removeLayer(image_basemap);
    choix_carte.checked = true;
  }
  compteur_basemap += 1;
}

function verifCocheDepart(event){
  event.preventDefault();
  if(!choix_depart.checked){
    map.removeLayer(carte_departements);
    depart_added = false;
  }
  if(choix_depart.checked && depart_added == false){
    map.addLayer(carte_departements);
    carte_departements.setZIndex(1);
    depart_added = true;
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Fonction qui affiche les couches cochées dans l'onglet de gestion des couches à droite
function affichage_couche(checkbox, color, sourceVecteur, nvL_couche){

  var gestionLayer = document.querySelector('#gestion_couche');

  var nouvelleCouche = document.createElement('div');
  nouvelleCouche.classList.add('couche');
  nouvelleCouche.style.height = '100px';
  nouvelleCouche.style.background = 'antiquewhite';
  nouvelleCouche.style.border = '1px black solid';
  nouvelleCouche.style.display = 'grid';
  nouvelleCouche.style.gridTemplateColumns = '15% 22% 21% 21% 21%';
  nouvelleCouche.style.gridTemplateRows = '30% 40% 30%';

  // Cercle coloré
  var cercle = document.createElement('div');
  cercle.style.borderRadius = '50%';
  cercle.style.gridRow = '1';
  cercle.style.gridColumn = '1';
  cercle.style.backgroundColor = color;
  cercle.style.margin = 'auto';
  cercle.style.width = '20px';
  cercle.style.height = '20px';
  nouvelleCouche.appendChild(cercle);

  // Nom de la couche
  var name = checkbox.dataset.layerName;
  var titre = document.createElement("div");
  titre.textContent = name;
  titre.style.gridRow = '1';
  titre.style.gridColumn = '2 / 6';
  titre.style.fontSize = 'auto';
  titre.style.margin = '2px';
  nouvelleCouche.appendChild(titre);

  // Barre transparence
  var transparence = document.createElement("input");
  transparence.setAttribute('type', 'range');
  transparence.setAttribute('min', '0');
  transparence.setAttribute('max', '1');
  transparence.setAttribute('value', '0');
  transparence.setAttribute('step', '0.01');
  transparence.style.gridRow = '2';
  transparence.style.gridColumn = '3 / 6';
  transparence.style.width = '60%';
  transparence.style.height = '60%';
  transparence.style.margin = 'auto';
  nouvelleCouche.appendChild(transparence);

  

  // Visualisation
  var img_visu = document.createElement("img");
  img_visu.src = "images/oeil_ouvert.png";
  img_visu.style.gridRow = '3';
  img_visu.style.gridColumn = '3';
  img_visu.style.width = '60%';
  img_visu.style.height = '60%';
  img_visu.style.objectFit = 'contain';
  nouvelleCouche.appendChild(img_visu);

  // Bouton Information
  var img_info = document.createElement("img");
  img_info.src = "images/information.png";
  img_info.style.gridRow = ' 2 / 4';
  img_info.style.gridColumn = '1 / 3';
  img_info.style.width = '40%';
  img_info.style.height = '40%';
  img_info.style.objectFit = 'contain';
  img_info.style.margin = 'auto';
  nouvelleCouche.appendChild(img_info);

  // Recentrer
  var img_center = document.createElement("img");
  img_center.src = "images/center.png";
  img_center.style.gridRow = '3';
  img_center.style.gridColumn = '4';
  img_center.style.width = '60%';
  img_center.style.height = '60%';
  img_center.style.objectFit = 'contain';
  nouvelleCouche.appendChild(img_center);

  // Poubelle
  var img_bin = document.createElement("img");
  img_bin.src = "images/recycle-bin.png";
  img_bin.style.gridRow = '3';
  img_bin.style.gridColumn = '5';
  img_bin.style.width = '60%';
  img_bin.style.height = '60%';
  img_bin.style.objectFit = 'contain';
  nouvelleCouche.appendChild(img_bin);


  gestionLayer.appendChild(nouvelleCouche);

  // Suppression de la couche dans la gestion des couches si l'utilisateur décoche la couche
  checkbox.addEventListener('change', function() {
    if (!checkbox.checked) {
      // Si la checkbox est décochée, on supprime la div parent
      gestionLayer.removeChild(nouvelleCouche);
    }
  });

  // Gestion des interactions
  // Transparence
  transparence.addEventListener('change', function() {
    var opacite = 1-(transparence.value)
    console.log(opacite);
    nvL_couche.setOpacity(opacite);
  });
  // Poubelle
  img_bin.addEventListener('click', function() {
    //clique_poubelle(data_couche, checkbox, gestionLayer, nouvelleCouche)
    checkbox.click();
    gestionLayer.removeChild(nouvelleCouche);
  });
  // Recentrer
  img_center.addEventListener('click', function(){
    zoom_sur_source(sourceVecteur)
  });
  // Visible ou non
  var compteur_visible = 0
  img_visu.addEventListener('click', function() {
    compteur_visible+=1;

    if(compteur_visible%2==0){
      nvL_couche.setVisible(true);
      img_visu.src = "images/oeil_ouvert.png";
    }else{
      nvL_couche.setVisible(false);
      img_visu.src = "images/oeil_barre.png";
    }
  });
  // Informations
  img_info.addEventListener('click',function(){
    informations(checkbox)
    /*var div = document.querySelector('#informations');
    closeDiv(div);*/
  });


}


// Onglet informatif
function informations(checkbox){

  var informations = document.querySelector('#informations');
  informations.style.display = 'grid';
  informations.style.fontFamily = "'Alatsi', sans-serif";
  
  // Nom de la couche
  var name = checkbox.dataset.completeLayerName;
  var titre = document.createElement('div');
  titre.textContent = name;
  titre.style.fontWeight = 'bold';
  titre.style.fontSize = 'large';
  titre.style.textAlign = 'center';
  titre.style.alignSelf = 'center';
  titre.style.gridRow = '2';
  titre.style.gridColumn = '2 / 20';
  titre.style.border = '1px black solid';
  informations.appendChild(titre);

  // Image de la couche
  var image = checkbox.dataset.image;
  var img_sat = document.createElement("img");
  img_sat.src = image;
  img_sat.style.gridRow = '4 / 11';
  img_sat.style.gridColumn = '2 / 10';
  img_sat.style.width = '100%';
  img_sat.style.height = '100%';
  img_sat.style.objectFit = 'contain';
  informations.appendChild(img_sat);

  // Thème SGPE
  var them_sgpe = checkbox.dataset.sgpe;
  var sgpe = document.createElement('div');
  sgpe.textContent = 'Thématique SGPE : ' + them_sgpe;
  sgpe.style.alignSelf = 'center';
  sgpe.style.gridRow = '4';
  sgpe.style.gridColumn = '11 / 20';
  sgpe.style.border = '1px black solid';
  informations.appendChild(sgpe);

  // Date de la couche
  var date_passage = checkbox.dataset.date;
  var date = document.createElement('div');
  date.textContent = 'Date : ' + date_passage;
  date.style.alignSelf = 'center';
  date.style.gridRow = '6';
  date.style.gridColumn = '11 / 20';
  date.style.border = '1px black solid';
  informations.appendChild(date);

  // Satellite / Bande
  var sat = checkbox.dataset.satellite;
  var satellite = document.createElement('div');
  satellite.textContent = 'Satellite / Bande : ' + sat;
  satellite.style.alignSelf = 'center';
  satellite.style.gridRow = '8';
  satellite.style.gridColumn = '11 / 20';
  satellite.style.border = '1px black solid';
  informations.appendChild(satellite);

  // Temps de revisite
  var tps_revisite = checkbox.dataset.revisite;
  var revisite = document.createElement('div');
  revisite.textContent = 'Temps de revisite : ' + tps_revisite;
  revisite.style.alignSelf = 'center';
  revisite.style.gridRow = '10';
  revisite.style.gridColumn = '11 / 20';
  revisite.style.border = '1px black solid';
  informations.appendChild(revisite);

  // Coût
  var gratuite = checkbox.dataset.cout;
  var cout = document.createElement('div');
  cout.textContent = 'Coût : ' + gratuite;
  cout.style.alignSelf = 'center';
  cout.style.gridRow = '12';
  cout.style.gridColumn = '11 / 20';
  cout.style.border = '1px black solid';
  informations.appendChild(cout);

  // Résolution
  var resol = checkbox.dataset.resolution;
  var resolution = document.createElement('div');
  resolution.textContent = 'Résolution : ' + resol;
  resolution.style.alignSelf = 'center';
  resolution.style.gridRow = '12';
  resolution.style.gridColumn = '2 / 10';
  resolution.style.border = '1px black solid';
  informations.appendChild(resolution);

  // Description
  var desc = checkbox.dataset.description;
  var description = document.createElement('div');
  description.textContent = "Description : " + desc;
  description.style.gridRow = '14 / 20';
  description.style.gridColumn = '2 / 20';
  description.style.border = '1px black solid';
  informations.appendChild(description);

  addCloseButton(informations);

  informations.style.zIndex = '20';
}

function addCloseButton(informations) {

  var closeButton = document.createElement("span");
  closeButton.style.gridRow = '1';
  closeButton.style.gridColumn = '20';
  closeButton.style.textAlign = 'center';
  closeButton.innerHTML = "&times;"; // Le symbole de la croix
  closeButton.style.fontSize = '40px';
  closeButton.classList.add("close-button");
  closeButton.addEventListener('mouseover', function(){
    closeButton.style.cursor = 'pointer';
  });
  informations.appendChild(closeButton);
  // Cacher la div
  closeButton.addEventListener("click", function() {
    informations.style.display = "none";
    informations.innerHTML = "";
  });
}

//Ferme une div quand l'on clique à l'extérieur de celle-ci
function closeDiv(div){

  document.addEventListener('click', function(event) {
    if (!div.contains(event.target)) {
      div.style.display = 'none';
    }
  });
}
